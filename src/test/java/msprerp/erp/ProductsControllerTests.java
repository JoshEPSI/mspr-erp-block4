package msprerp.erp;

import msprerp.erp.controller.CustomersController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import msprerp.erp.controller.ProductsController;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductsControllerTests {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private ProductsController productsController;

    @Test
    @DisplayName("TestUnitaireGetProductsAPI")
    public void testGetProductsUnit() {
        // Given
        String expectedProducts = "[{\"id\":1,\"name\":\"Product 1\"},{\"id\":2,\"name\":\"Product 2\"}]";

        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(String.class))).thenReturn(expectedProducts);

        ProductsController productsController = new ProductsController(restTemplate);

        // When
        ResponseEntity<String> actualResponse = productsController.getProducts();

        // Then
        Assertions.assertEquals(expectedProducts, actualResponse.getBody());
    }


    @Test
    @DisplayName("TestUnitaireGetProductAPI")
    public void testGetProductUnit() {
        // Given
        String expectedResponse = "{\"id\":2,\"name\":\"Product 2\"}";
        Long productId = 2L;

        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(String.class))).thenReturn(expectedResponse);

        ProductsController productsController = new ProductsController(restTemplate);

        // When
        ResponseEntity<String> actualResponse = productsController.getProduct(productId);

        // Then
        Assertions.assertEquals(expectedResponse, actualResponse.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetProducts")
    public void testGetProducts() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        // Configurer la réponse du client REST Mock
        String productsJson = "[{\"id\":1,\"name\":\"Product 1\"},{\"id\":2,\"name\":\"Product 2\"}]";
        Mockito.when(restTemplateMock.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products", String.class))
                .thenReturn(productsJson);

        productsController = new ProductsController(restTemplateMock);

        ResponseEntity<String> response = productsController.getProducts();

        // Vérifier que la méthode a renvoyé une réponse 200 OK
        assertEquals(HttpStatus.OK, response.getStatusCode());

        // Vérifier que la réponse contient les produits attendus
        String expectedResponse = productsJson;
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetProductsErreur")
    public void testGetProductsErreur() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        // Configurer le client REST Mock pour renvoyer une exception lors de l'appel à l'API Mock
        Mockito.when(restTemplateMock.getForObject(Mockito.anyString(), Mockito.eq(String.class)))
                .thenThrow(new RestClientException("Erreur lors de la requête REST"));

        productsController = new ProductsController(restTemplateMock);

        ResponseEntity<String> response = productsController.getProducts();

        // Vérifier que la méthode a renvoyé une réponse 500 INTERNAL SERVER ERROR
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());

        // Vérifier que la réponse contient le message d'erreur attendu
        String expectedResponse = "Une erreur s'est produite : Erreur lors de la requête REST";
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetProduct")
    public void testGetProduct() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        Long productId = 1L;
        // Définir la réponse à retourner par le serveur mocké
        String product = "{\"createdAt\":\"2023-02-19T13:42:19.010Z\",\"name\":\"Rex Bailey\",\"details\":{\"price\":\"659.00\",\"description\":\"The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J\",\"color\":\"red\"},\"stock\":12059,\"id\":\"1\"}";
        Mockito.when(restTemplateMock.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/" + productId, String.class))
                .thenReturn(product);

        productsController = new ProductsController(restTemplateMock);

        ResponseEntity<String> response = productsController.getProduct(productId);

        // Vérifier que la méthode a renvoyé une réponse 200 OK
        assertEquals(HttpStatus.OK, response.getStatusCode());

        // Vérifier que la réponse contient les produits attendus
        String expectedResponse = product;
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetProductErreur")
    public void testGetProductError() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        Long productId = 1L;
        // Configurer le client REST Mock pour renvoyer une exception lors de l'appel à l'API Mock
        Mockito.when(restTemplateMock.getForObject(Mockito.anyString(), Mockito.eq(String.class)))
                .thenThrow(new RestClientException("Erreur lors de la requête REST"));

        productsController = new ProductsController(restTemplateMock);

        ResponseEntity<String> response = productsController.getProduct(productId);

        // Vérifier que la méthode a renvoyé une réponse 500 INTERNAL SERVER ERROR
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());

        // Vérifier que la réponse contient le message d'erreur attendu
        String expectedResponse = "Une erreur s'est produite : Erreur lors de la requête REST";
        assertEquals(expectedResponse, response.getBody());
    }
}
