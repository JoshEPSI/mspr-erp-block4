package msprerp.erp;

import msprerp.erp.controller.CustomersController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomersControllerTests {


    @Mock
    private RestTemplate restTemplate;

    @Mock
    private CustomersController customersController;

    @Test
    @DisplayName("Test d'Intégration Get Customers API")
    public void testGetCustomersInt() {
        //Given
        String baseUrl = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers";
        RestTemplate restTemplate = new RestTemplate();

        //When
        String response = restTemplate.getForObject(baseUrl, String.class);

        //Then
        Assertions.assertNotNull(response);
        Assertions.assertFalse(response.isEmpty());
    }

    @Test
    @DisplayName("TestUnitaireGetCustomersAPI")
    public void testGetCustomersUnit() {
        // Given
        String expectedCustomers = "[{\"id\":\"1\",\"name\":\"OuiOui\",\"email\":\"ouioui@oui.com\"},{\"id\":\"2\",\"name\":\"NonNon\",\"email\":\"nonnon@non.com\"}]";
        when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(String.class))).thenReturn(expectedCustomers);
        CustomersController customersController = new CustomersController(restTemplate);

        // When
        ResponseEntity<String> response = customersController.getCustomers();
        String actualCustomers = response.getBody();

        // Then
        assertEquals(expectedCustomers, actualCustomers);
    }


//METHODESDETESTSPOURLESPRODUITS

    @Test
    @DisplayName("Testd'IntégrationGetProductsAPI")
    public void testGetProductsInt() {
        //Given
        String baseUrl = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/8/orders/8/products";
        RestTemplate restTemplate = new RestTemplate();

        //When
        String response = restTemplate.getForObject(baseUrl, String.class);

        //Then
        Assertions.assertNotNull(response);
        Assertions.assertFalse(response.isEmpty());
    }

    @Test
    @DisplayName("TestUnitaireGetProductsAPI")
    public void testGetProductsUnit() {
        //Given
        Long customerId = 1L;
        Long orderId = 1L;
        String expectedProducts = "[{\"id\":\"1\",\"name\":\"Product1\",\"price\":10},{\"id\":\"2\",\"name\":\"Product2\",\"price\":20}]";
        when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(String.class))).thenReturn(expectedProducts);
        CustomersController customersController = new CustomersController(restTemplate);

        //When
        ResponseEntity<String> actualProducts = customersController.getProducts(customerId, orderId);

        //Then
        assertEquals(HttpStatus.OK, actualProducts.getStatusCode());
        assertEquals(expectedProducts, actualProducts.getBody());
    }

//METHODESDETESTSPOURLESCOMMANDES

    @Test
    @DisplayName("Testd'IntégrationGetOrdersAPI")
    public void testGetOrdersInt() {
        //Given
        String baseUrl = "https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/8/orders";
        RestTemplate restTemplate = new RestTemplate();

        //When
        ResponseEntity<String> response = restTemplate.getForEntity(baseUrl, String.class);

        //Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertNotNull(response.getBody());
        Assertions.assertFalse(response.getBody().isEmpty());
    }

    @Test
    @DisplayName("TestUnitaireGetOrdersAPI")
    public void testGetOrdersUnit() {
        // Given
        String expectedOrders = "[{\"createdAt\":\"2023-02-19T22:36:57.004Z\",\"id\":\"9\",\"customerId\":\"9\"},{\"createdAt\":\"2023-02-20T10:08:35.226Z\",\"id\":\"59\",\"customerId\":\"9\"}]";
        Long customerId = 9L;
        when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(String.class))).thenReturn(expectedOrders);
        CustomersController customersController = new CustomersController(restTemplate);

        // When
        ResponseEntity<String> actualOrders = customersController.getOrders(customerId);

        // Then
        assertEquals(expectedOrders, actualOrders.getBody());
        assertEquals(HttpStatus.OK, actualOrders.getStatusCode());
    }

    @Test
    @DisplayName("TestUnitaireGetCustomers")
    public void testGetCustomers() {
        // Créer un mock pour la classe RestTemplate
        RestTemplate restTemplateMock = mock(RestTemplate.class);

        // Configurer le mock pour qu'il renvoie une réponse HTTP avec une liste de clients
        String mockResponse = "[{\"id\":1,\"name\":\"Alice\"},{\"id\":2,\"name\":\"Bob\"}]";
        when(restTemplateMock.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers", String.class))
                .thenReturn(mockResponse);

        // Créer une instance de CustomerController avec le mock RestTemplate
        customersController = new CustomersController(restTemplateMock);

        // Appeler la méthode getCustomers() et vérifier que la réponse est correcte
        ResponseEntity<String> responseEntity = customersController.getCustomers();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(mockResponse, responseEntity.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetCustomersErreur")
    public void testGetCustomersErreur() {
        // Créer un mock pour la classe RestTemplate
        RestTemplate restTemplateMock = mock(RestTemplate.class);

        // Configurer le client REST Mock pour renvoyer une exception lors de l'appel à l'API Mock
        Mockito.when(restTemplateMock.getForObject(Mockito.anyString(), Mockito.eq(String.class)))
                .thenThrow(new RestClientException("Erreur lors de la requête REST"));

        customersController = new CustomersController(restTemplateMock);

        ResponseEntity<String> response = customersController.getCustomers();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        // Vérifier que la réponse contient les produits attendus
        String expectedResponse = "Une erreur s'est produite : Erreur lors de la requête REST";
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetOrders")
    public void testGetOrders() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        Long customersId = 123L;

        // Configurer la réponse du client REST Mock
        String ordersJson = "[{\"id\":1,\"name\":\"Order 1\"},{\"id\":2,\"name\":\"Order 2\"}]";
        when(restTemplateMock.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + customersId + "/orders", String.class))
                .thenReturn(ordersJson);

        customersController = new CustomersController(restTemplateMock);

        ResponseEntity<String> response = customersController.getOrders(customersId);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        String expectedResponse = ordersJson;
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetOrdersErreur")
    public void testGetOrdersErreur() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        Long customersId = 123L;

        // Configurer le client REST Mock pour renvoyer une exception lors de l'appel à l'API Mock
        Mockito.when(restTemplateMock.getForObject(Mockito.anyString(), Mockito.eq(String.class)))
                .thenThrow(new RestClientException("Erreur lors de la requête REST"));

        customersController = new CustomersController(restTemplateMock);

        ResponseEntity<String> response = customersController.getOrders(customersId);

        // Vérifier que la méthode a renvoyé une réponse 500 INTERNAL SERVER ERROR
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        // Vérifier que la réponse contient les produits attendus
        String expectedResponse = "Une erreur s'est produite : Erreur lors de la requête REST";
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetProducts")
    public void testGetProducts() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        // Configurer la réponse du client REST Mock
        String productsJson = "[{\"id\":1,\"name\":\"Product 1\"},{\"id\":2,\"name\":\"Product 2\"}]";
        Mockito.when(restTemplateMock.getForObject(Mockito.anyString(), Mockito.eq(String.class)))
                .thenReturn(productsJson);

        Long customerId = 123L;
        Long orderId = 456L;

        customersController = new CustomersController(restTemplateMock);

        ResponseEntity<String> response = customersController.getProducts(customerId, orderId);

        // Vérifier que la méthode a renvoyé une réponse 200 OK
        assertEquals(HttpStatus.OK, response.getStatusCode());

        // Vérifier que la réponse contient les produits attendus
        String expectedResponse = productsJson;
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    @DisplayName("TestUnitaireGetProductsErreur")
    public void testGetProductsErreur() {
        // Créer un client REST Mock
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        // Configurer le client REST Mock pour renvoyer une exception lors de l'appel à l'API Mock
        Mockito.when(restTemplateMock.getForObject(Mockito.anyString(), Mockito.eq(String.class)))
                .thenThrow(new RestClientException("Erreur lors de la requête REST"));

        Long customerId = 123L;
        Long orderId = 456L;
        customersController = new CustomersController(restTemplateMock);

        ResponseEntity<String> response = customersController.getProducts(customerId, orderId);

        // Vérifier que la méthode a renvoyé une réponse 500 INTERNAL SERVER ERROR
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());

        // Vérifier que la réponse contient le message d'erreur attendu
        String expectedResponse = "Une erreur s'est produite : Erreur lors de la requête REST";
        assertEquals(expectedResponse, response.getBody());
    }
}
