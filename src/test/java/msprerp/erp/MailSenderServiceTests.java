package msprerp.erp;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import msprerp.erp.model.Users;
import msprerp.erp.service.MailSenderService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class MailSenderServiceTests {

    @Mock
    private JavaMailSender javaMailSender;

    @Mock
    private MailSenderService mailSenderService;

    private Users user;

    @Test
    public void testMailWithQRCode() throws Exception {
        // Arrange
        String toEmail = "example@example.com";
        String body = "Test body";
        String subject = "Test subject";
        String jwt = "jwt";

        MimeMessage message = javaMailSender.createMimeMessage();

        // Act
        mailSenderService.mailWithQRCode(toEmail, body, subject, jwt);
        javaMailSender.send(message);
        // Assert
        verify(mailSenderService).mailWithQRCode(toEmail, body, subject, jwt);
        verify(javaMailSender).send(message);
    }
}