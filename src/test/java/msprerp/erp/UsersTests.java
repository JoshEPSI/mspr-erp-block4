package msprerp.erp;

import msprerp.erp.model.Users;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsersTests {

    private Users user;

    // Define a test email address and token
    private final String email = "test@example.com";
    private final String token = "test-token-1234567890";

    @BeforeEach
    void setUp() {
        user = new Users();
        user.setEmail(email);
        user.setToken(token);
    }

    @Test
    void generateToken_ShouldReturnNonNullToken() {
        // Arrange
        user.setToken(null);

        // Act
        String token = user.generateToken();

        // Assert
        assertNotNull(token);
    }

    @Test
    void generateToken_ShouldBeVerifiedWithSameSecretKey() {
        // Arrange
        user.setToken(null);
        user.generateToken();

        // Act
        boolean isValid = user.validateToken(user.getToken());

        // Assert
        assertTrue(isValid);
    }

    @Test
    void validateToken_ShouldReturnTrueForValidToken() {
        // Arrange
        user.generateToken();

        // Act
        boolean isValid = user.validateToken(user.getToken());

        // Assert
        assertTrue(isValid);
    }

    @Test
    void validateToken_ShouldReturnFalseForExpiredToken() {
        // Arrange
        user.generateToken();

        // Act
        boolean isValid = user.validateToken(user.getToken());

        // Assert
        assertTrue(isValid);
    }

    @Test
    void validateToken_ShouldReturnFalseForInvalidToken() {
        // Arrange
        String invalidToken = "invalid-token-1234567890";

        // Act
        boolean isValid = user.validateToken(invalidToken);

        // Assert
        assertFalse(isValid);
    }

    @Test
    void validateToken_ShouldReturnFalseForNullToken() {
        // Arrange
        String nullToken = null;

        // Act
        boolean isValid = user.validateToken(nullToken);

        // Assert
        assertFalse(isValid);
    }

    @Test
    void setPassword_ShouldEncodePassword() {
        // Arrange
        String password = "test-password-1234567890";

        // Act
        user.setToken(password);

        // Assert
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        assertFalse(passwordEncoder.matches(password, user.getToken()));
    }
}