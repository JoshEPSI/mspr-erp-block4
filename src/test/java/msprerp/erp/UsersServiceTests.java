package msprerp.erp;

import msprerp.erp.model.Users;
import msprerp.erp.repository.UsersRepository;
import msprerp.erp.service.MailSenderService;
import msprerp.erp.service.UsersService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsersServiceTests {

    @Autowired
    private UsersService usersService;

    @MockBean
    private UsersRepository usersRepository;

    private MailSenderService mailSenderService;

    private Users user;

    @BeforeEach
    void setUp() {
        user = new Users();
        user.setId(1L);
        user.setEmail("test@test.com");
        user.setToken("test-token");
    }

    @Test
    public void testFindAll() {
        // Arrange
        List<Users> users = new ArrayList<>();
        Users user1 = new Users();
        user1.setEmail("test1@example.com");
        users.add(user1);
        Users user2 = new Users();
        user2.setEmail("test2@example.com");
        users.add(user2);
        Mockito.when(usersRepository.findAll()).thenReturn(users);

        // Act
        List<Users> result = usersService.findAll();

        // Assert
        Assert.assertEquals(2, result.size());
        Assert.assertEquals("test1@example.com", result.get(0).getEmail());
        Assert.assertEquals("test2@example.com", result.get(1).getEmail());
    }


    @Test
    void testCreateUserSuccess() throws Exception {
        when(usersRepository.findByEmail(anyString())).thenReturn(null);
        when(usersRepository.save(user)).thenReturn(user);

        Users createdUser = usersService.createUser(user);

        assertNotNull(createdUser);
        assertEquals(user.getEmail(), createdUser.getEmail());
        assertEquals(user.getToken(), createdUser.getToken());

        verify(usersRepository, times(1)).findByEmail(anyString());
        verify(usersRepository, times(1)).save(user);
    }

    @Test
    void testCreateUserFailure() throws Exception {
        when(usersRepository.findByEmail(user.getEmail())).thenReturn(user);

        assertThrows(Exception.class, () -> {
            usersService.createUser(user);
        });

        verify(usersRepository, times(1)).findByEmail(user.getEmail());
        verify(usersRepository, never()).save(user);
    }

    @Test
    void testFindUserByToken() {
        when(usersRepository.findByToken(user.getToken())).thenReturn(user);

        Users foundUser = usersService.findUserBytoken(user.getToken());

        assertNotNull(foundUser);
        assertEquals(user.getEmail(), foundUser.getEmail());
        assertEquals(user.getToken(), foundUser.getToken());

        verify(usersRepository, times(1)).findByToken(user.getToken());
    }

    @Test
    void testFindUserByEmail() {
        when(usersRepository.findByEmail(user.getEmail())).thenReturn(user);

        Users foundUser = usersService.findUserMyEmail(user.getEmail());

        assertNotNull(foundUser);
        assertEquals(user.getEmail(), foundUser.getEmail());
        assertEquals(user.getToken(), foundUser.getToken());

        verify(usersRepository, times(1)).findByEmail(user.getEmail());
    }

    @Test
    void testUpdateUserToken() {
        when(usersRepository.save(user)).thenReturn(user);

        Users updatedUser = usersService.updateUserToken(user);

        assertNotNull(updatedUser);
        assertEquals(user.getEmail(), updatedUser.getEmail());
        assertEquals(user.getToken(), updatedUser.getToken());

        verify(usersRepository, times(1)).save(user);
    }
}