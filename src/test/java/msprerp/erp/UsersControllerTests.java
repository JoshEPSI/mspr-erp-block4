package msprerp.erp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import msprerp.erp.controller.UsersController;
import msprerp.erp.model.Users;
import msprerp.erp.service.MailSenderService;
import msprerp.erp.service.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsersControllerTests {

    private MockMvc mockMvc;

    @Mock
    private UsersService usersService;

    @Mock
    private MailSenderService emailSenderService;

    @InjectMocks
    private UsersController usersController;

    public UsersControllerTests() {
    }

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(usersController).build();
    }

    @Test
    public void testCreateUser() throws Exception {
        Users user = new Users();
        user.setEmail("test@test.com");
        user.setToken("testToken");

        when(usersService.createUser(any(Users.class))).thenReturn(user);

        mockMvc.perform(post("/add/user")
                        .content(asJsonString(user))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value(user.getEmail()))
                .andExpect(jsonPath("$.token").value(user.getToken()));
    }

    private static String asJsonString(final Object obj) throws JsonProcessingException {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testFindAllUsers() {
        // Créer une liste d'utilisateurs fictive pour simuler la réponse de la couche service
        List<Users> userList = new ArrayList<>();
        userList.add(new Users());
        userList.add(new Users());

        // Mock de la méthode de service pour retourner la liste fictive d'utilisateurs
        Mockito.when(usersService.findAll()).thenReturn(userList);

        // Appeler la méthode à tester
        ResponseEntity<?> response = usersController.findAllUsers();

        // Vérifier que la réponse est OK et que le corps de la réponse contient la liste d'utilisateurs fictive
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(userList, response.getBody());
    }

    @Test
    public void testConnectToInterfaceSuccess() throws Exception {
        Users user = new Users();
        user.setEmail("test@test.com");
        user.setToken("testToken");

        when(usersService.findUserMyEmail(any(String.class))).thenReturn(user);

        mockMvc.perform(get("/connect/user")
                        .param("email", "test@test.com")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("0"));
    }

    @Test
    public void testConnectToInterfaceUserNotFound() throws Exception {
        when(usersService.findUserMyEmail(any(String.class))).thenReturn(null);

        mockMvc.perform(get("/connect/user")
                        .param("email", "test@test.com")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void testConnectToInterfaceWithExpiredToken() {
        // Créer un utilisateur fictif avec un token expiré
        Users user = new Users();
        Mockito.when(usersService.findUserMyEmail("john.doe@example.com")).thenReturn(user);

        // Appeler la méthode à tester
        ResponseEntity<?> response = usersController.connectToInterface("john.doe@example.com");

        // Vérifier que la réponse est OK et que le corps de la réponse est égal à 0
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0, response.getBody());
    }

    @Test
    public void testConnectToInterfaceWithValidToken() {
        // Créer un utilisateur fictif avec un token valide
        Users user = new Users();
        Mockito.when(usersService.findUserMyEmail("john.doe@example.com")).thenReturn(user);

        // Appeler la méthode à tester
        ResponseEntity<?> response = usersController.connectToInterface("john.doe@example.com");

        // Vérifier que la réponse est OK et que le corps de la réponse est égal à 0
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(0, response.getBody());
    }

    @Test
    void testConnectToInterface1() {
        // Création d'un utilisateur avec un token
        Users user = new Users();
        user.setEmail("test@example.com");
        user.setToken("token123");
        Mockito.when(usersService.findUserMyEmail("test@example.com")).thenReturn(user);

        // Envoi du mail avec le token
        ResponseEntity<String> response = usersController.connectToInterface1("test@example.com");

        // Vérification du code de retour et du message
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Mail envoyé avec un code QR en pièce joint", response.getBody());
    }

    @Test
    void testConnectToInterface1Erreur() {
        // Envoi d'une adresse e-mail invalide
        ResponseEntity<String> response = usersController.connectToInterface1("invalid_email");

        // Vérification du code de retour et du message
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertEquals("User not found", response.getBody());
    }

    @Test
    void testFindUserByTokenPass() {
        // Création d'un utilisateur avec un token
        Users user = new Users();
        user.setToken("token123");
        Mockito.when(usersService.findUserBytoken("token123")).thenReturn(user);

        // Recherche de l'utilisateur par token
        ResponseEntity<?> response = usersController.findUserByToken("token123");

        // Vérification du code de retour et du corps de la réponse
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody() instanceof String);
        String userJson = (String) response.getBody();
        assertNotNull(userJson);
    }

    @Test
    void testFindUserByTokenFail() {
        // Recherche d'un utilisateur avec un token inexistant
        ResponseEntity<?> response = usersController.findUserByToken("non_existing_token");

        // Vérification du code de retour et du message d'erreur
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
        assertEquals("User not found", response.getBody());
    }
}
