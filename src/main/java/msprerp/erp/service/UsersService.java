package msprerp.erp.service;

import msprerp.erp.model.Users;
import msprerp.erp.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Service
public class UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private MailSenderService mailSenderService;


    public List<Users> findAll() {
        return usersRepository.findAll();

    }

    public Users createUser(Users user) throws Exception {
        String email = user.getEmail();

        // Check if a user with this email already exists
        Users existingUser = usersRepository.findByEmail(email);
        if(existingUser != null){
             throw new Exception("User with email " + email + " already exists");
        }

        String token = user.generateToken();
        System.out.println(token + "  " + "TOKEN IN SERVICEE");
        // send email with a QR code attachment
        String text = "Email de creation" ;
        String subject = "Email de confirmation creation:";
        mailSenderService.sendSimpleEmail(email, text, subject);

        return usersRepository.save(user);
    }

    public Users findUserBytoken(String token) {
        return usersRepository.findByToken(token);
    }

    public Users findUserMyEmail(String email) {
        return usersRepository.findByEmail(email);
    }

    public Users updateUserToken(Users user) {
        if (!user.validateToken(user.getToken())) {
            String newToken = user.generateToken();
            user.setToken(newToken);
            usersRepository.save(user);
        }

        return user;
    }

}

