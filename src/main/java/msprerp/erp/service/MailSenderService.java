 package msprerp.erp.service;


 import com.google.zxing.BarcodeFormat;
 import com.google.zxing.client.j2se.MatrixToImageConfig;
 import com.google.zxing.client.j2se.MatrixToImageWriter;
 import com.google.zxing.common.BitMatrix;
 import com.google.zxing.qrcode.QRCodeWriter;
 import msprerp.erp.model.Users;
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.core.io.FileSystemResource;
 import org.springframework.mail.SimpleMailMessage;
 import org.springframework.mail.javamail.JavaMailSender;
 import org.springframework.mail.javamail.MimeMessageHelper;
 import org.springframework.stereotype.Service;

 import javax.mail.internet.MimeMessage;
 import java.io.File;
 import java.nio.file.FileSystems;
 import java.nio.file.Path;


 @Service
public class MailSenderService {
   @Autowired
   private JavaMailSender javaMailSender;


     public void sendSimpleEmail(String toEmail, String subject, String text) throws Exception {

         SimpleMailMessage message = new SimpleMailMessage();
         message.setFrom("tresorkouakou@gmail.com");
         message.setTo(toEmail);
         message.setSubject(subject);
         message.setText(text);
         javaMailSender.send(message);
         System.out.println("Email de creation d'un utilisateur");

    }


     public void mailWithQRCode(String toEmail, String body, String subject, String jwt) throws Exception{

         Users user = new Users();
         String token = user.generateToken();
         System.out.println();
         MimeMessage message = javaMailSender.createMimeMessage();
         MimeMessageHelper helper = new MimeMessageHelper(message, true);

         helper.setFrom("tresorkouakou@gmail.com");
         helper.setTo(toEmail);
         helper.setText(body);
         helper.setSubject(subject);

         QRCodeWriter qrCodeWriter = new QRCodeWriter();
         BitMatrix bitMatrix = qrCodeWriter.encode(jwt, BarcodeFormat.QR_CODE, 300, 300);

         Path path = FileSystems.getDefault().getPath("qrcode.png");
         MatrixToImageConfig config = new MatrixToImageConfig();
         MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path.toFile(),config);

         FileSystemResource file = new FileSystemResource(new File("qrcode.png"));
         helper.addAttachment("qrcode.png", file);

         javaMailSender.send(message);
         System.out.println("E-mail envoyé avec un code QR joint..");
     }

}
