package msprerp.erp.model;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


import javax.persistence.*;
import java.util.Date;



@Entity
@Table(name = "users")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name="type")
    private String type;

    @Column(name = "token")
    private String token;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getType(){
        return type;
    }

    public String setType(String type){
        return this.type = type;
    }

    /*
    Expiration time in milliseconds (2 hours)
    private static final long EXPIRATION_TIME = 2 * 60 * 60 * 1000;
     */

    //Expiration time in milliseconds (3 minutes)
    private static final long EXPIRATION_TIME = (long) 3 * 60 * 1000;
    // Secret key for signing the JWT
    public static final String SECRET_KEY = "password";

    public String generateToken() {
        Date expirationDate = new Date(System.currentTimeMillis() + EXPIRATION_TIME);

        // Build the JWT
        String jwt = Jwts.builder()
                .setSubject(email)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
        System.out.println(jwt + "        " + "TOOOOKEN");
        setToken(jwt);
        return jwt;

    }


    public boolean validateToken(String jwt) {
        if (jwt == null || jwt.isEmpty()) {
            return false;
        }
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(jwt)
                    .getBody();
            Date expirationDate = claims.getExpiration();
            return expirationDate.after(new Date());
        } catch (Exception e) {
            return false;
        }
    }
}