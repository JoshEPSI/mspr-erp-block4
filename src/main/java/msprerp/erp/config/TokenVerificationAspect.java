package msprerp.erp.config;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import msprerp.erp.controller.CustomersController;
import msprerp.erp.controller.ProductsController;
import msprerp.erp.model.Users;
import msprerp.erp.service.UsersService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static msprerp.erp.model.Users.SECRET_KEY;
import javax.servlet.http.HttpServletRequest;

import java.io.IOException;

import static msprerp.erp.model.Users.SECRET_KEY;

@Aspect
@Component
public class TokenVerificationAspect {
    @Autowired
    private UsersService usersService;

    @Autowired
    private HttpServletRequest request;

    @Around("@annotation(msprerp.erp.config.TokenVerification)")
    public Object verifyTokenAndUserType(ProceedingJoinPoint joinPoint) throws Throwable {
        String token = request.getHeader("Authorization");
        String email = null;
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET_KEY)
                    .parseClaimsJws(token.replace("Bearer ", ""))
                    .getBody();
            email = claims.getSubject();
        } catch (Exception e) {
            throw new IOException("Token expired");
        }

        // Check if the user exists and has the necessary permissions
        Users user = usersService.findUserMyEmail(email);
        if (user == null) {
            throw new IOException("User not found");
        }
        String userType = user.getType();
        if (joinPoint.getTarget() instanceof CustomersController) {
            if (!userType.equals("CRM")) {
                throw new IOException("User doesn't have the necessary permissions");
            }
        } else if (joinPoint.getTarget() instanceof ProductsController) {
            if (!userType.equals("ERP")) {
                throw new IOException("User doesn't have the necessary permissions");
            }
        }

        return joinPoint.proceed();
    }
}
