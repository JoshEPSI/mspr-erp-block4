package msprerp.erp.controller;

import msprerp.erp.config.TokenVerification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/customers")
public class CustomersController {

    @Autowired
    private RestTemplate restTemplate;

    public CustomersController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/")
    @TokenVerification(value = "CRM")
    /**
     * Enoncé : La liste des clients issue du CRM est accessible via l’API REST
     * /customers
     *
     * Accès à la liste des clients :
     * http://localhost:8081/customers/
     */
    public ResponseEntity<String> getCustomers() {
        try {
            String customers = restTemplate.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers", String.class);
            return ResponseEntity.ok(customers);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite : " + e.getMessage());
        }
    }

    @GetMapping("/{id}/orders")
    @TokenVerification(value = "CRM")
    /**
     * Enoncé : La liste des commandes d’un client est accessible via l’API REST
     * /customers/<customer id>/orders
     *
     * Accès à la liste des commandes d'un client :
     * http://localhost:8081/customers/2/orders
     */
    public ResponseEntity<String> getOrders(@PathVariable("id") Long customersId) {
        try {
            String orders = restTemplate.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + customersId + "/orders", String.class);
            return ResponseEntity.ok(orders);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite : " + e.getMessage());
        }
    }

    @GetMapping("/{customerId}/orders/{orderId}/products")
    @TokenVerification(value = "CRM")
    /**
     * Enoncé : La liste de produits d’une commande est accessible via l’API REST
     * /customers/<customer id>/orders/<orderid>/products
     *
     * Accès à la liste des produits d'un client :
     * http://localhost:8081/customers/2/orders/2/products
     */
    public ResponseEntity<String> getProducts(@PathVariable("customerId") Long customerId, @PathVariable("orderId") Long orderId) {
        try {
            String products = restTemplate.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/customers/" + customerId + "/orders/" + orderId + "/products", String.class);
            return ResponseEntity.ok(products);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite : " + e.getMessage());
        }
    }

}
