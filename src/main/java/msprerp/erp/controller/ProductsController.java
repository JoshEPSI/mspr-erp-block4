package msprerp.erp.controller;


import msprerp.erp.config.TokenVerification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/products")
public class ProductsController {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    public ProductsController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    @GetMapping("/")
    @TokenVerification(value = "ERP")
    /**
     * Enoncé : La liste des produits issue de l’ERP est accessible via l’API REST
     * /products
     *
     * Accès à la liste des produits issue de l'ERP
     * http://localhost:8081/products/
     */
    public ResponseEntity<String> getProducts() {
        try {
            String products = restTemplate.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products", String.class);
            return ResponseEntity.ok().body(products);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite : " + e.getMessage());
        }
    }


    @GetMapping("/{productId}")
    @TokenVerification(value = "ERP")
    /**
     * Enoncé : La liste des produits issue de l’ERP est accessible via l’API REST
     * /products/<product id>
     *
     * Accès au détail d'un produit issue de l'API REST
     * http://localhost:8081/products/2
     */
    public ResponseEntity<String> getProduct(@PathVariable("productId") Long productId) {
        try {
            String product = restTemplate.getForObject("https://615f5fb4f7254d0017068109.mockapi.io/api/v1/products/" + productId, String.class);
            return ResponseEntity.ok().body(product);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Une erreur s'est produite : " + e.getMessage());
        }
    }
}
