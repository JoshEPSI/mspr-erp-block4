package msprerp.erp.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import msprerp.erp.model.Users;
import msprerp.erp.service.MailSenderService;
import msprerp.erp.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController {

    @Autowired
    private UsersService usersService;

    @Autowired
    MailSenderService mailSenderService;
    @PostMapping("add/user")
    public ResponseEntity<?> createUser(@RequestBody Users user) throws Exception {
        System.out.println(user + "        " + "user");
        Users createdUser = usersService.createUser(user);
        return ResponseEntity.ok().body(createdUser);
    }
    @GetMapping("/users")
    public ResponseEntity<?> findAllUsers() {
        List<Users> users = usersService.findAll();
        return ResponseEntity.ok().body(users);
    }

    @GetMapping("connect/user")
    public ResponseEntity<?> connectToInterface(@RequestParam("email") String email) {
        try {
            Users user = usersService.findUserMyEmail(email);

            if(user == null){
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User not found");
            }
            String token = user.getToken();
            if (token != null && user.validateToken(token)) {
                // Token is valid, don't send email
                return ResponseEntity.ok(1);
            } else {
                // Token is expired or not set, generate a new one
                usersService.updateUserToken(user);
                String newToken = user.getToken();
                mailSenderService.mailWithQRCode(email, "Veuillez scanner ce code QR pour vous connecter à l'interface.", "Nouvelle connexion CaféErp", newToken);
                return ResponseEntity.ok(0);
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur d'envoi de l'e-mail: " + e.getMessage());
        }
    }

    @GetMapping("/user")
    public ResponseEntity<?> findUserByToken(@RequestParam("token") String token) {
        try {
            Users user = usersService.findUserBytoken(token);
            if(user == null){
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User not found");
            }
            ObjectMapper objectMapper = new ObjectMapper();
            String userJson;
            try {
                userJson = objectMapper.writeValueAsString(user);
            } catch (JsonProcessingException e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
            return ResponseEntity.ok(userJson);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping("/connect")
    public ResponseEntity<String> connectToInterface1(@RequestParam("email") String email) {
        try {
            Users user = usersService.findUserMyEmail(email);

            if(user == null){
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User not found");
            }
            mailSenderService.mailWithQRCode(email, "Veuillez scanner ce code QR pour vous connecter à l'interface.", "Nouvelle connexion CaféErp", user.getToken());
            return ResponseEntity.ok("Mail envoyé avec un code QR en pièce joint");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Erreur d'envoi de l'e-mail: " + e.getMessage());
        }
    }

}